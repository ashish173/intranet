Intranet Software
=================

This is an open-source intranet site. Feel free to use it for your office.

Supported Modules:

* Employee Management
* Leave Management
* Vendor Management
* Company Document Management
* Project Status Management (Status and teams)
* Integration with bonus.ly
* Integration with blog

Coming soon(future):

* Newsletter Management
* Interview Management
* Knowledge base Management 
