require 'spec_helper'

describe Vendor do
    context 'Validation specs' do 
    	it { should have_fields(:company, :category) }
    	it { should validate_presence_of(:company, :category) }
    end
end
