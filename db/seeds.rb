# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#
puts "Beginning seeds"
User.destroy_all
puts "cleared destroy all"
admin = User.new(email: "with.rishav_admin@gmail.com", password: "intranet123", role: "Admin", status: "approved")
puts "created admin role"
hr = User.new(email: "with.rishav_hr@gmail.com", password: "intranet123", role: "HR", status: "approved")
puts "created hr role"
employee = User.new(email: "with.rishav_emp@gmail.com", password: "intranet123", role: "Employee", status: "approved")
puts "created employee role"
admin.build_public_profile(first_name: "Rishav", last_name: "Admin")
puts "built public profile for admin"
hr.build_public_profile(first_name: "Rishav", last_name: "HR")
puts "built public profile for hr"
employee.build_public_profile(first_name: "Rishav", last_name: "Emp")
hr.build_private_profile(date_of_joining: Date.parse('1-1-2015'))
employee.build_private_profile(date_of_joining: Date.parse('1-1-2015'))
admin.save
puts "admin saved"
hr.save
employee.save
